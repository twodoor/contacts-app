export class Contact {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;

  constructor(contact: ContactI) {
    const {
      firstName = '',
      lastName = '',
      email = '',
      phoneNumber = ''
    } = contact;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
  }
}

export interface ContactI {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
}
