export class UserLogin {
  email: string;
  password: string;

  constructor(contact: UserLoginI) {
    const {
      email = '',
      password = ''
    } = contact;
    this.email = email;
    this.password = password;
  }
}

export interface UserLoginI {
  email: string;
  password: string;
}
