export class User {
  email: string;
  firstName: string;
  lastName: string;

  constructor(contact: UserI) {
    const {
      email = '',
      firstName = '',
      lastName = ''
    } = contact;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}

export interface UserI {
  email?: string;
  firstName?: string;
  lastName?: string;
}
