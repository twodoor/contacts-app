import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from 'src/app/shared/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedInUser: User;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userService.getLoggedInUser().subscribe((user: User) => {
      if (!user) {
        this.router.navigate(['login']);
      }
      this.loggedInUser = user;
    });
  }

  onLogoutClicked() {
    this.userService.logout();
  }

}
