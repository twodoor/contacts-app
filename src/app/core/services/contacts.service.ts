import { Injectable } from '@angular/core';
import { of, BehaviorSubject, Observable } from 'rxjs';
import { CONTACTS } from '../mocks/contacts';
import { Contact, ContactI } from 'src/app/shared/models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  private contacts$: BehaviorSubject<Contact[]> = new BehaviorSubject<Contact[]>([]);

  constructor() {
    this.loadContacts();
  }

  loadContacts() {
    of(CONTACTS).subscribe((data: ContactI[]) => {
      const contacts = data.map((contact: ContactI) => new Contact(contact));
      this.contacts$.next(contacts);
    });
  }

  getContacts(): Observable<Contact[]> {
    return this.contacts$;
  }

  addContact(newContact: Contact): Observable<Contact> {
    // This shold be done by the backend
    const contacts = this.contacts$.getValue();
    if (contacts.find((contact: Contact) => contact.email === newContact.email)) {
      return of(null);
    } else {
      contacts.unshift(newContact);
      this.contacts$.next(contacts);
      return of(newContact);
    }
  }

  editContact(editedContact: Contact): Observable<Contact> {
    let indexOfContactToEdit = this.contacts$.getValue()
      .findIndex(contact => contact.email === editedContact.email);
    if (indexOfContactToEdit === -1) {
      indexOfContactToEdit = this.contacts$.getValue()
        .findIndex(contact => contact.phoneNumber === editedContact.phoneNumber);
    }

    this.contacts$.getValue()[indexOfContactToEdit] = editedContact;
    this.contacts$.next(this.contacts$.getValue());
    return of(editedContact);
  }

  removeContact(contactToRemove: Contact) {
    this.contacts$.next(this.contacts$.getValue()
      .filter((contact: Contact) => contact.email !== contactToRemove.email)
    );
  }
}
