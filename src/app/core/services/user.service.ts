import { Injectable } from '@angular/core';
import { UserLogin, UserLoginI } from 'src/app/shared/models/user-login';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { User, UserI } from 'src/app/shared/models/user';
import { USER } from '../mocks/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public loggedInUser$: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor() { }

  login(userData: UserLogin): Observable<User> {
    console.log(userData);
    // ToDo: do some login
    // return http.post<UserLogin>('api/login', (result) => {
    const result = new User(USER);
    this.loggedInUser$.next(result);
    return of(result);
    // })
  }

  register(userData): Observable<User> {
    const result = new User(USER);
    this.loggedInUser$.next(result);
    return of(result);
  }

  logout() {
    this.loggedInUser$.next(null);
  }

  getLoggedInUser(): Observable<User> {
    return this.loggedInUser$;
  }
}
