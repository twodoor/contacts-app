import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsListComponent } from './pages/contacts-list/contacts-list.component';
import { MatTableModule } from '@angular/material/table';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { MatIconModule, MatInputModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    ContactsListComponent,
    ContactFormComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSortModule
  ],
  entryComponents: [
    ContactFormComponent
  ]
})
export class ContactsModule { }
