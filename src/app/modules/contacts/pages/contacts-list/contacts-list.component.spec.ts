import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsListComponent } from './contacts-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { MatToolbarModule, MatMenuModule, MatFormFieldModule, MatIconModule, MatTooltipModule, MatTableModule, MatDialogModule, MatDialogRef, MatInputModule, MAT_DIALOG_DATA } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

describe('ContactsListComponent', () => {
  let component: ContactsListComponent;
  let fixture: ComponentFixture<ContactsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactsListComponent],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        CommonModule,
        MatToolbarModule,
        MatMenuModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        RouterModule,
        MatTooltipModule,
        MatTableModule,
        MatDialogModule,
        MatInputModule
      ], providers: [{
        provide: MatDialogRef,
        useValue: {}
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
