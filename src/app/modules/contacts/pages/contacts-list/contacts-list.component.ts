import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactsService } from 'src/app/core/services/contacts.service';
import { Contact } from 'src/app/shared/models/contact';
import { MatDialog, MatTableDataSource, MatDialogConfig, MatSort } from '@angular/material';
import { ContactFormComponent } from '../../components/contact-form/contact-form.component';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'phone', 'actions'];
  contacts: MatTableDataSource<Contact>;
  defaultFormConfig: MatDialogConfig = { disableClose: true, minWidth: 400 };

  constructor(private contactsService: ContactsService, private dialog: MatDialog) { }

  ngOnInit() {
    this.contactsService.getContacts().subscribe((contacts: Contact[]) => {
      this.contacts = new MatTableDataSource(contacts);
      this.contacts.sort = this.sort;
    });
  }

  onEditClicked(contact: Contact) {
    this.dialog.open(ContactFormComponent, { ...this.defaultFormConfig, data: contact });
  }

  onRemoveClicked(contact: Contact) {
    if (confirm(`Are you sure you want to delete ${contact.firstName} ${contact.lastName}?`)) {
      this.contactsService.removeContact(contact);
    }
  }

  onAddContactClicked() {
    this.dialog.open(ContactFormComponent, this.defaultFormConfig);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contacts.filter = filterValue.trim().toLowerCase();
  }
}
