import { async, ComponentFixture, TestBed, flush, fakeAsync } from '@angular/core/testing';

import { ContactFormComponent } from './contact-form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import {
  MatToolbarModule, MatMenuModule, MatTooltipModule,
  MatIconModule, MatFormFieldModule, MatDialogModule,
  MatDialogRef, MatInputModule, MAT_DIALOG_DATA
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactsService } from 'src/app/core/services/contacts.service';
import { CONTACTS } from 'src/app/core/mocks/contacts';
import { of, Observable } from 'rxjs';
import { Contact } from 'src/app/shared/models/contact';

describe('ContactFormComponent', () => {
  let component: ContactFormComponent;
  let fixture: ComponentFixture<ContactFormComponent>;
  let dialogRef: MatDialogRef<ContactFormComponent>;
  let contactsService: ContactsService;
  const MatDialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);

  class ContactsServiceMock {
    private contacts$ = of(CONTACTS);

    getContacts(): Observable<Contact[]> {
      return this.contacts$;
    }

    editContact(editedContact: Contact): Observable<Contact> {
      return of(editedContact);
    }

    addContact(newContact: Contact): Observable<Contact> {
      return of(newContact);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactFormComponent],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        CommonModule,
        MatToolbarModule,
        MatMenuModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        RouterModule,
        MatTooltipModule,
        MatDialogModule,
        MatInputModule
      ], providers: [{
        provide: MatDialogRef,
        useValue: MatDialogRefSpy
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }, {
        provide: contactsService,
        useValue: ContactsServiceMock
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dialogRef = fixture.debugElement.injector.get(MatDialogRef) as MatDialogRef<ContactFormComponent>;
    contactsService = fixture.debugElement.injector.get(ContactsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a new contact if no dialogData was provided', () => {
    component.newContact = true;
    const formData = {
      firstName: 'john',
      lastName: 'doe',
      email: 'abc@email.com',
      phoneNumber: '123'
    };
    component.contactForm.patchValue(formData);
    const contactsServiceSpy = spyOn(contactsService, 'addContact').and.callThrough();
    component.onSubmit();
    expect(contactsService.addContact).toHaveBeenCalledWith(formData);
  });

  it('should edit a contact if dialogData is provided', () => {
    component.contactData = {
      firstName: 'john',
      lastName: 'doe',
      email: 'abc@email.com',
      phoneNumber: '123'
    };
    component.ngOnInit();
    const contactsServiceSpy = spyOn(contactsService, 'editContact').and.callThrough();
    component.onSubmit();
    expect(contactsService.editContact).toHaveBeenCalledWith(component.contactData);
  });

  it('should close the dialog as soon as the contact is created', fakeAsync(() => {
    component.onSubmit();
    flush();
    expect(dialogRef.close).toHaveBeenCalled();
  }));

  it('should invalidate the form if email is not valid', () => {
    component.newContact = true;
    const formData = {
      firstName: 'john',
      lastName: 'doe',
      email: 'abc',
      phoneNumber: '123'
    };
    component.contactForm.patchValue(formData);
    expect(component.contactForm.get('email').errors).toEqual({ email: true });
    expect(component.contactForm.invalid).toBe(true);
  });

  it('should invalidate the form if no name provided', () => {
    component.newContact = true;
    const formData = {
      firstName: '',
      lastName: '',
      email: 'abc@email.com',
      phoneNumber: '123'
    };
    component.contactForm.patchValue(formData);
    expect(component.contactForm.get('firstName').errors).toEqual({ required: true });
    expect(component.contactForm.get('lastName').errors).toEqual({ required: true });
    expect(component.contactForm.invalid).toBe(true);
  });
});
