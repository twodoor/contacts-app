import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { UserLogin } from 'src/app/shared/models/user-login';
import { User } from 'src/app/shared/models/user';
import { ContactsService } from 'src/app/core/services/contacts.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Contact } from 'src/app/shared/models/contact';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;
  duplicateEntry = false;
  newContact = true;

  constructor(
    public dialogRef: MatDialogRef<ContactFormComponent>,
    @Inject(MAT_DIALOG_DATA) public contactData: Contact,
    private formBuilder: FormBuilder,
    private contactsService: ContactsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.required]
    });

    if (this.contactData) {
      this.newContact = false;
      this.contactForm.patchValue(this.contactData);
    }
  }

  onCancelClicked() {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.newContact) {
      this.contactsService.addContact(this.contactForm.value).subscribe((addedContact: Contact) => {
        if (addedContact) {
          this.duplicateEntry = false;
          this.dialogRef.close();
        } else {
          this.duplicateEntry = true;
        }
      });
    } else {
      this.contactsService.editContact(this.contactForm.value).subscribe((editedContact: Contact) => {
        this.dialogRef.close();
      });
    }
  }

}
