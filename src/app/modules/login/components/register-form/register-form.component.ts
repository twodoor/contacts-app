import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required, this.matchConfirmPassword.bind(this)]]
    });
  }

  matchConfirmPassword(control: AbstractControl) {
    if (this.registerForm) {
      const pass = this.registerForm.get('password').value;
      const confirmPassword = control.value;

      return pass === confirmPassword ? null : { passwordNoMatch: true };
    }
  }

  onSubmit() {
    this.userService.register(this.registerForm.value).subscribe(result => {
      if (result) {
        this.router.navigate(['contacts']);
      }
    });
  }

}
