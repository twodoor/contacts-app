import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import {
  MatToolbarModule,
  MatMenuModule, MatFormFieldModule,
  MatIconModule, MatTooltipModule,
  MatTableModule, MatDialogModule, MatInputModule, MatTabsModule, MatCardModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginFormComponent } from '../../components/login-form/login-form.component';
import { RegisterFormComponent } from '../../components/register-form/register-form.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, LoginFormComponent, RegisterFormComponent],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        CommonModule,
        MatToolbarModule,
        MatMenuModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        RouterModule,
        MatTooltipModule,
        MatTableModule,
        MatDialogModule,
        MatInputModule,
        MatTabsModule,
        MatCardModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
